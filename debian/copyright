Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: TinyOWS
Upstream-Contact: Barbara Philippot, Olivier Courtin and the MapServer team.
Source: https://github.com/mapserver/tinyows
Disclaimer: The package is in non-free because it contains OGC schemas which
 limit modification via the terms of the Document Notice.

Files: *
Copyright: 2007-2024, Barbara Philippot - Olivier Courtin
License: MIT/X11

Files: schema/ows/1.0.0/*
Copyright: 2005, 2010, Open Geospatial Consortium, Inc.
License: OGC-Software

Files: schema/gml/3.1.1/base/*
Copyright: 2001, 2005, 2010, Open Geospatial Consortium, Inc.
License: OGC-Software

Files: schema/gml/3.1.1/smil/*
Copyright: 1998-2001, W3C (MIT, INRIA, Keio)
License: OGC-Software

Files: schema/gml/2.1.2/*
Copyright: 2001-2002, OGC
License: OGC-Software

Files: schema/filter/1.0.0/*
Copyright: 2002, 2010, Open Geospatial Consortium, Inc.
License: OGC-Software

Files: schema/filter/1.1.0/*
Copyright: 2002-2004, 2010, Open Geospatial Consortium, Inc.
License: OGC-Software

Files: schema/xlink/1.0.0/*
Copyright: 2001, OGC
License: OGC-Software

Files: schema/wfs/1.0.0/*
Copyright: 2002, 2010, Open Geospatial Consortium, Inc.
License: OGC-Software

Files: schema/wfs/1.1.0/*
Copyright: 2002, 2010, Open Geospatial Consortium, Inc.
License: OGC-Software

Files: debian/*
Copyright: 2010, Intevation GmbH <christopher.bertels@intevation.de>
License: GPL-3+

License: MIT/X11
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the Software is 
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 IN THE SOFTWARE.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: OGC-Software
 Software Notice 
 .
 This OGC work (including software, documents, or other related items) is being
 provided by the copyright holders under the following license. By obtaining,
 using and/or copying this work, you (the licensee) agree that you have read,
 understood, and will comply with the following terms and conditions:
 .
 Permission to use, copy, and modify this software and its documentation, with
 or without modification, for any purpose and without fee or royalty is hereby
 granted, provided that you include the following on ALL copies of the software
 and documentation or portions thereof, including modifications, that you make:
 .
   1. The full text of this NOTICE in a location viewable to users of the
      redistributed or derivative work.
   2. Any pre-existing intellectual property disclaimers, notices, or terms
      and conditions. If none exist, a short notice of the following form
      (hypertext is preferred, text is permitted) should be used within the
      body of any redistributed or derivative code:
      "Copyright © [$date-of-document] Open Geospatial Consortium, Inc. All
       Rights Reserved. http://www.opengeospatial.org/ogc/legal"
      (Hypertext is preferred, but a textual representation is permitted.)
   3. Notice of any changes or modifications to the OGC files, including the
      date changes were made. (We recommend you provide URIs to the location
      from which the code is derived.) 
 .
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS
 MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR
 PURPOSE OR THAT THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE
 ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR DOCUMENTATION.
 .
 The name and trademarks of copyright holders may NOT be used in advertising
 or publicity pertaining to the software without specific, written prior
 permission. Title to copyright in this software and any associated
 documentation will at all times remain with copyright holders.

