Source: tinyows
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Pirmin Kalberer <pi_deb@sourcepole.ch>,
           Bas Couwenberg <sebastic@debian.org>
Section: non-free/web
Priority: optional
Build-Depends: debhelper-compat (= 13),
               flex,
               libfcgi-dev,
               libfl-dev,
               libpq-dev,
               libxml2-dev (>= 2.8.0),
               postgis
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/tinyows
Vcs-Git: https://salsa.debian.org/debian-gis-team/tinyows.git
Homepage: http://www.mapserver.org/tinyows/
Rules-Requires-Root: no

Package: tinyows
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: apache2,
            libapache2-mod-fcgid,
            postgis
Suggests: cgi-mapserver,
          libapache2-mod-mapcache
Description: lightweight and fast WFS-T server
 TinyOWS is a lightweight and fast implementation of the OGC WFS-T
 specification. Web Feature Service (WFS) allows querying and retrieval
 of features. The transactional profile (WFS-T) allows inserting, updating
 or deleting such features.
 .
 From a technical point of view WFS-T is a Web Service API in front
 of a spatial database. TinyOWS is deeply tied to PostgreSQL/PostGIS
 because of this.
 .
 TinyOWS implements strictly OGC standards and pass successfully all
 WFS OGC CITE tests (and even beta ones).
 .
 TinyOWS is part of MapServer Suite, but provided as a distinct module
 (i.e you could use it in conjunction with MapServer and MapCache, or
 as a standalone app). But both MapServer and TinyOWS could use the same
 configuration file, if you want to (or native TinyOWS XML config file).
